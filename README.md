# PServiceBus
Service Bus for PHP inspired by [NServiceBus](https://particular.net/nservicebus).

You can read the principals of usage and why we need it from their documentation :)

Documentation is bad. Ask in issues if you need help.


## Installation
```bash
composer require gdx/p-service-bus
```
## USAGE
So far no great examples
Please look For examples in https://gitlab.com/GDXbsv/pservicebus/-/tree/master/TestApp
How to start to use it in your project:
- Initialization example https://gitlab.com/GDXbsv/pservicebus/-/blob/master/src/Setup.php
- How I do it in tests https://gitlab.com/GDXbsv/pservicebus/-/blob/master/tests/Integration/IntegrationTestCase.php#L45

## Features
- Saga/Aggregate consume command/event produce event.
- Bus allow to send command or publish event.
- CoroutineBus allow to send multiple commands or publish multiple events
- Doctrine integration (Saga persistence, transactional messages(OutBox pattern), onlyOnce control)
- Automatically init all the resources for you
- ServiceBus as main entry point

##Init
```bash
p-service-bus:init
```
##Send/Publish command/event

`\GDXbsv\PServiceBus\Bus\ServiceBus` implements all the Bus interfaces.

If you have a couple of message use `Bus`
```php
#command
$bus->send(new TestCommand());
#event
$bus->publish(new TestEvent());
```

If you have many messages use `CoroutineBus`
```php
#command
$coroutine = $coroutineBus->sendCoroutine();
$coroutine->send(new Message(new TestCommand(1), CommandOptions::record()));
$coroutine->send(new Message(new TestCommand(2), CommandOptions::record()));
$coroutine->send(null);
#event
$coroutine = $coroutineBus->publishCoroutine();
$coroutine->send(new Message(new TestEvent(1), EventOptions::record()));
$coroutine->send(new Message(new TestEvent(2), EventOptions::record()));
$coroutine->send(null);
```

## Consume
To start consuming run the command
```bash
p-service-bus:transport:consume  memory
```
where:
- memory is transport name

## Handlers
You can make any method as handler just use PHP Attribute

```php
<?php declare(strict_types=1);

use GDXbsv\PServiceBus\Bus\Handling\Handle;
use GDXbsv\PServiceBus\Message\MessageHandlerContext;

/**
 * @internal
 */
final class Handlers
{
    public string $result = '';

    #[Handle('memory')]
    public function handleCommand(TestMultiHandlersCommand $command, MessageHandlerContext $context): void
    {
        $this->result .= '||' . $command->name;
    }
    #[Handle(transportName: 'memory', retries: 3, timeoutSec: 100)]
    public function anyNameFunction(Test1Event $event, MessageHandlerContext $context): void
    {
        $this->result .= '||' . $event->name;
    }
}

```

##External events
Send outside to subscribed clients (for example from SNS). 
Or receive from outside where we subscribed (for example to SNS).

```php
<?php
declare(strict_types=1);

use GDXbsv\PServiceBus\Message\ExternalIn;
use GDXbsv\PServiceBus\Message\ExternalOut;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
#[ExternalOut(transportName: 'memory-external', externalName: 'test.external_1_event')]
final class ExternalOutEvent
{
}

#[ExternalIn(transportName: 'memory-external', externalName: 'test.external_1_event')]
final class ExternalInEvent
{
}

```

## Replay
Sometimes something goes wrong and you want to replay certain events. For this use replay annotations.

```php
<?php
declare(strict_types=1);

use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBus\Message\Replay\Replay;
use GDXbsv\PServiceBusTestApp\Handling\Test1Event;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 *
 * @psalm-import-type ReplayOutput from \GDXbsv\PServiceBus\Message\Replay\Replaying
 */
final class ReplayForEvent
{
    /**
     * @return ReplayOutput
     */
    #[Replay(replayName: 'testReplay')]
    public function anyName(): \Traversable {
        for ($i=1; $i<=5; ++$i) {
            yield new Message(new Test1Event(), EventOptions::record());
        }
    }
}

```

Then use command to start replay 
```bash
p-service-bus:message:replay testReplay "\GDXbsv\PServiceBusTestApp\Handling\Handlers::handle2Event1" memory
```
where:
- testReplay is replay name from Attribute
- "\GDXbsv\PServiceBusTestApp\Handling\Handlers::handle2Event1" is className + :: + methodName
- memory is transport name

## Saga

```php
<?php declare(strict_types=1);

use GDXbsv\PServiceBus\Bus\Handling\Handle;
use GDXbsv\PServiceBus\Id;
use GDXbsv\PServiceBus\Message\MessageHandlerContext;
use GDXbsv\PServiceBus\Message\TimeSpan;
use GDXbsv\PServiceBus\Saga\Saga;
use GDXbsv\PServiceBus\Saga\SagaPropertyMapper;
use Doctrine\ORM\Mapping as ORM;
use GDXbsv\PServiceBusTestApp\Saga\TestSagaCommand;
use GDXbsv\PServiceBusTestApp\Saga\TestSagaMapStringCommand;
use GDXbsv\PServiceBusTestApp\Saga\TestsSagaInEvent;
use GDXbsv\PServiceBusTestApp\Saga\TestsSagaOutputEvent;


/**
 * @ORM\Entity()
 * @final
 */
final class TestSaga extends Saga
{
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $string;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $value;

    public static function configureHowToFindSaga(SagaPropertyMapper $mapper): void
    {
        $mapper
            ->mapSaga(new \ReflectionProperty(TestSaga::class, 'id'))
            ->toMessage(
                function (TestSagaCommand $command, MessageHandlerContext $context) {
                    return new Id($command->id);
                }
            )
            ->toMessage(
                function (TestsSagaInEvent $message, MessageHandlerContext $context) {
                    return new Id($message->string);
                }
            );
        $mapper
            ->mapSaga(new \ReflectionProperty(TestSaga::class, 'string'))
            ->toMessage(
                function (TestSagaMapStringCommand $command, MessageHandlerContext $context) {
                    return $command->string;
                }
            );
    }

    #[Handle('memory', 3)]
    public function testHandlerFunction(TestSagaCommand $command, MessageHandlerContext $context)
    {
        $this->string = $command->string;
        $this->timeout(new TestsSagaOutputEvent('testHandlerFunction'), TimeSpan::fromSeconds(0));
    }

    #[Handle('memory', 3)]
    public function testListeningFunction(TestsSagaInEvent $event, MessageHandlerContext $context)
    {
        $this->string = $event->string;
        $this->value = $event->value;
        $this->apply(new TestsSagaOutputEvent('testListeningFunction'));
    }

    #[Handle('memory', 3)]
    public function handleTestSagaMapStringCommand(
        TestSagaMapStringCommand $command,
        MessageHandlerContext $context
    ) {
        $this->apply(new TestsSagaOutputEvent($this->id->toString()));
    }
}

```

where:
- configureHowToFindSaga describe how to find saga
- `#[Handle('memory', 3)]` set methods as handlers

You can create custom finders by using `#[SagaFind]` attribute, for example:

```php
<?php declare(strict_types=1);

use Doctrine\ORM\EntityManager;
use GDXbsv\PServiceBus\Message\MessageOptions;
use GDXbsv\PServiceBus\Saga\SagaFind;
use GDXbsv\PServiceBusTestApp\Saga\CustomDoctrineSearchEvent;
use GDXbsv\PServiceBusTestApp\Saga\TestSaga;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class CustomDoctrineSagaFinder
{
    public function __construct(
        private EntityManager $em
    ) {
    }

    #[SagaFind]
    public function findByMultipleFields(
        CustomDoctrineSearchEvent $event,
        MessageOptions $messageOptions
    ): TestSaga {
        $qb = $this->em->createQueryBuilder();
        $qb
            ->from(TestSaga::class, 'saga')
            ->select('saga')
            ->where($qb->expr()->eq('saga.string', ':propertyValue'))
            ->setParameter(':propertyValue', $event->string);
        $saga = $qb->getQuery()->getSingleResult();

        return $saga;
    }
}
```

#Transport
So far implemented only InMemoryTransport and RabbitMq(BunnyTransport) and SQS and SNS.
But you can adapt any of yours by implementing 2 interfaces
```php
interface Transport
{
    /**
     * @return \Generator<int, void, Envelope|null, void>
     */
    public function sending(): \Generator;

    /**
     * @return \Generator<int, Envelope, Result\Ok<null, mixed>|Result\Err<mixed, \Exception>, void>
     */
    public function receive(int $limit = 0): \Generator;

    public function stop(): void;
}
interface TransportSynchronisation
{
    public function sync(): void;
}
```
Please pay attention that `sync()` method for an external bus MUST subscribe you on external message name if you want it to happen automatically.

###bunny transport

For bunny transport it is different internal and external transports. External use exchanges and pub/sub.

See https://gitlab.com/GDXbsv/pservicebus/-/tree/master/src/Transport/Bunny

###SQS-SNS transport

For SQS-SNS transports SNS is only for external messages.

See https://gitlab.com/GDXbsv/pservicebus/-/tree/master/src/Transport/Sqs
See https://gitlab.com/GDXbsv/pservicebus/-/tree/master/src/Transport/Sns


