<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Handling;

use GDXbsv\PServiceBus\Bus\Handling\Handle;
use GDXbsv\PServiceBus\Message\MessageHandlerContext;

/**
 * @internal
 */
final class Handlers
{
    public string $result = '';

    #[Handle('memory')]
    public function handleMulti1(TestMultiHandlersCommand $command, MessageHandlerContext $context): void
    {
        $this->result .= '||' . $command->name;
    }
    #[Handle('memory')]
    public function handleMulti2(TestMultiHandlersCommand $command, MessageHandlerContext $context): void
    {
        $this->result .= '||' . $command->name;
    }
    #[Handle('memory')]
    public function __invoke(Test1Command $command, MessageHandlerContext $context): void
    {
        $this->result .= '||' . $command->name;
    }
    #[Handle('memory')]
    public function anyNameFunction(Test1Event $event, MessageHandlerContext $context): void
    {
        $this->result .= '||' . $event->name;
    }
    #[Handle('memory')]
    public function handle2Event1(Test1Event $event, MessageHandlerContext $context): void
    {
        $this->result .= '||' . $event->name;
    }
}
