<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\HandlingExternal;

use GDXbsv\PServiceBus\Message\ExternalOut;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
#[ExternalOut('memory-external', 'test-from.external_in_event')]
final class ExternalInEvent
{
    public function __construct(
        public string $name = 'ExternalInEvent'
    ) {
    }
}
