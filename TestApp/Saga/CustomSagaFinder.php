<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Saga;

use GDXbsv\PServiceBus\Id;
use GDXbsv\PServiceBus\Message\MessageOptions;
use GDXbsv\PServiceBus\Saga\SagaFind;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class CustomSagaFinder
{
    #[SagaFind]
    public function findByMultipleFields(
        CustomSearchEvent $event,
        MessageOptions $messageOptions
    ): TestSaga {
        // DO some fancy search in SQL for example

        return new TestSaga(new Id('customSagaId'));
    }
}
