<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Saga;

use GDXbsv\PServiceBus\Bus\Handling\Handle;
use GDXbsv\PServiceBus\Id;
use GDXbsv\PServiceBus\Message\MessageHandlerContext;
use GDXbsv\PServiceBus\Message\TimeSpan;
use GDXbsv\PServiceBus\Saga\Saga;
use GDXbsv\PServiceBus\Saga\SagaPropertyMapper;
use GDXbsv\PServiceBusTestApp\Handling\Test1Event;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity()
 * @final
 */
final class TestSaga extends Saga
{
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    public ?string $string;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    public ?string $value;

    public static function configureHowToFindSaga(SagaPropertyMapper $mapper): void
    {
        $mapper
            ->mapSaga(new \ReflectionProperty(TestSaga::class, 'id'))
            ->toMessage(
                function (TestSagaCommand $command, MessageHandlerContext $context) {
                    return new Id($command->id);
                }
            )
            ->toMessage(
                function (TestsSagaInEvent $message, MessageHandlerContext $context) {
                    return new Id($message->string);
                }
            )
            ->toMessage(
                function (TestsSagaInTimeoutEvent $message, MessageHandlerContext $context) {
                    return new Id($message->string);
                }
            )
            ->toMessage(
                function (TestsSagaOutputTimeoutEvent $message, MessageHandlerContext $context) {
                    return new Id($message->string);
                }
            );
        $mapper
            ->mapSaga(new \ReflectionProperty(TestSaga::class, 'string'))
            ->toMessage(
                function (TestSagaMapStringCommand $command, MessageHandlerContext $context) {
                    return $command->string;
                }
            );
    }

    #[Handle('memory', 3)]
    public function testHandlerFunction(TestSagaCommand $command, MessageHandlerContext $context)
    {
        $this->string = $command->string;
        $this->timeout(new TestsSagaOutputEvent('testHandlerFunction'), TimeSpan::fromSeconds(0));
    }

    #[Handle('memory', 3)]
    public function testListeningFunction(TestsSagaInEvent $event, MessageHandlerContext $context)
    {
        $this->string = $event->string;
        $this->value = $event->value;
        $this->apply(new TestsSagaOutputEvent('testListeningFunction'));
    }

    #[Handle('memory', 3)]
    public function testListeningWithTimeoutFunction(TestsSagaInTimeoutEvent $event, MessageHandlerContext $context)
    {
        $this->string = $event->string;
        $this->value = $event->value;
        $this->timeout(new TestsSagaOutputTimeoutEvent('testListeningWithTimeoutFunction'), TimeSpan::fromSeconds(3));
    }

    #[Handle('memory', 3)]
    public function testListeningAfterTimeoutFunction(
        TestsSagaOutputTimeoutEvent $event,
        MessageHandlerContext $context
    ) {
        $this->apply(new TestsSagaOutputEvent('testListeningAfterTimeoutFunction'));
    }

    #[Handle('memory', 3)]
    public function testListeningCustomSearchEvent(
        CustomSearchEvent $event,
        MessageHandlerContext $context
    ) {
        $this->apply(new TestsSagaOutputEvent($this->id->toString()));
    }

    #[Handle('memory', 3)]
    public function testListeningCustomDoctrienSearchEvent(
        CustomDoctrineSearchEvent $event,
        MessageHandlerContext $context
    ) {
        $this->apply(new TestsSagaOutputEvent($this->string));
    }

    #[Handle('memory', 3)]
    public function handleTestSagaMapStringCommand(
        TestSagaMapStringCommand $command,
        MessageHandlerContext $context
    ) {
        $this->apply(new TestsSagaOutputEvent($this->id->toString()));
    }
}
