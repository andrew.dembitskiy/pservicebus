<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Saga;

final class TestsSagaInEvent
{
    public $string = 'testSaga';
    public $value = 'secretValue';
}
