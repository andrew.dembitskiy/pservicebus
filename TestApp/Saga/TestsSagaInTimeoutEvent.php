<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Saga;

final class TestsSagaInTimeoutEvent
{
    public $string = 'testSaga';
    public $value = 'secretValue';
}
