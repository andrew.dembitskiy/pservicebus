<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusTestApp\Saga;

final class TestsSagaOutputTimeoutEvent
{
    public $string = 'testSaga';
    public string $result;

    public function __construct(string $result)
    {
        $this->result = $result;
    }
}
