<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus\Handling;

use GDXbsv\PServiceBus\Message\TimeSpan;

/**
 * @immutable
 * @psalm-immutable
 */
final class MessageHandleInstruction
{
    public function __construct(
        public string $messageClass,
        public string $transportName,
        public ?int $retries,
        public ?TimeSpan $timeout,
        public ?string $handlerName,
        public string $handlerMethodName,
    ) {
    }
}
