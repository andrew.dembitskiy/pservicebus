<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus\Handling\OnlyOnce;

use GDXbsv\PServiceBus\Message\Message;

interface OnlyOnceControl
{
    public function continue(Message $message): bool;
}
