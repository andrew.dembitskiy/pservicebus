<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus\Handling\OnlyOnce;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class OnlyOnceInitConsoleCommand extends Command
{
    protected static $defaultName = 'p-service-bus:only-once:init';
    private OnlyOnceInit $onlyOnceInit;

    public function __construct(OnlyOnceInit $onlyOnceInit)
    {
        $this->onlyOnceInit = $onlyOnceInit;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Init only once infrastructure.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $io = new SymfonyStyle($input, $output);

        $io->writeln('Start');
        $isCreated = $this->onlyOnceInit->init();
        if ($isCreated) {
            $io->writeln('Init Succeed');
            return self::SUCCESS;
        }
        $io->writeln('Init Failed');

        return self::FAILURE;
    }
}
