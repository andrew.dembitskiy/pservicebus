<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus\Handling;

use GDXbsv\PServiceBus\Message\TimeSpan;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 * @psalm-type MessageMap=array<class-string, list<MessageHandleInstruction>>
 */
final class ScrapeHandlers
{
    /**
     * @param class-string $className
     * @return MessageMap
     */
    public static function fromClass(string $className): array
    {
        return static::fromClasses([$className]);
    }

    /**
     * @param list<class-string> $classNames
     * @return MessageMap
     */
    public static function fromClasses(array $classNames): array
    {
        $handlers = [];

        foreach ($classNames as $className) {
            $reflectionClass = new \ReflectionClass($className);
            foreach ($reflectionClass->getMethods() as $method) {
                $attributes = $method->getAttributes(Handle::class);
                if (count($attributes) === 0) {
                    continue;
                }
                $arguments = $method->getParameters();
                assert(isset($arguments[0]));
                /** @var \ReflectionNamedType $type */
                $type = $arguments[0]->getType();
                /** @var class-string $messageType */
                $messageType = $type->getName();

                foreach ($attributes as $attribute) {
                    $handleAttr = $attribute->newInstance();

                    $handlers[$messageType][] = new MessageHandleInstruction(
                        $messageType,
                        $handleAttr->transportName,
                        $handleAttr->retries,
                        $handleAttr->timeoutSec ? TimeSpan::fromSeconds($handleAttr->timeoutSec) : null,
                        $className,
                        $method->getName(),
                    );
                }
            }
        }

        return $handlers;
    }
}
