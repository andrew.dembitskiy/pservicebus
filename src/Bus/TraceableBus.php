<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus;

use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBus\Message\CommandOptions;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBus\Transport\Transport;

final class TraceableBus implements Bus, ConsumeBus, CoroutineBus
{
    /** @var Message[] */
    private array $sent = [];
    /** @var Message[] */
    private array $published = [];
    private Bus $bus;
    private bool $tracing = false;
    private ConsumeBus $consumeBus;
    /** @var CoroutineBus */
    private CoroutineBus $coroutineBus;

    public function __construct(Bus $bus, ConsumeBus $consumeBus, CoroutineBus $coroutineBus)
    {
        $this->bus = $bus;
        $this->consumeBus = $consumeBus;
        $this->coroutineBus = $coroutineBus;
    }

    /**
     * @return Message[]
     */
    public function getEventMessages(): array
    {
        return $this->published;
    }

    /**
     * @param class-string $className
     */
    public function getEventMessageByClass(string $className): Message
    {
        foreach ($this->published as $message) {
            if ($message->payload::class === $className) {
                return $message;
            }
        }

        throw new \Exception("Did not find message with class '$className'");
    }

    /**
     * @return Message[]
     */
    public function getCommandMessages(): array
    {
        return $this->sent;
    }

    /**
     * @param class-string $className
     */
    public function getCommandMessageByClass(string $className): Message
    {
        foreach ($this->published as $message) {
            if ($message->payload::class === $className) {
                return $message;
            }
        }

        throw new \Exception("Did not find message with class '$className'");
    }

    /**
     * Start tracing.
     */
    public function trace(): void
    {
        $this->tracing = true;
    }

    public function send(object $message, ?CommandOptions $commandOptions = null): void
    {
        $this->bus->send($message, $commandOptions);

        if (!$this->tracing) {
            return;
        }

        $this->sent[] = new Message($message, $commandOptions ?? CommandOptions::record());
    }

    public function publish(object $message, ?EventOptions $eventOptions = null): void
    {
        $this->bus->publish($message, $eventOptions);

        if (!$this->tracing) {
            return;
        }

        $this->published[] = new Message($message, $eventOptions ?? EventOptions::record());
    }

    /**
     * @return \Generator<int, bool, Message<CommandOptions>|null, void>
     */
    public function sendCoroutine(): \Generator
    {
        $coroutine = $this->coroutineBus->sendCoroutine();
        $sent = true;
        while (true) {
            $message = (yield $sent);
            if ($message === null) {
                $coroutine->send(null);

                return;
            }
            $sent = $coroutine->send($message);
            if ($this->tracing) {
                $this->sent[] = $message;
            }
        }
    }

    /**
     * @return \Generator<int, bool, Message<EventOptions>|null, void>
     */
    public function publishCoroutine(): \Generator
    {
        $coroutine = $this->coroutineBus->publishCoroutine();
        $sent = true;
        while (true) {
            $message = (yield $sent);
            if ($message === null) {
                $coroutine->send(null);
                return;
            }
            $sent = $coroutine->send($message);
            if ($this->tracing) {
                $this->published[] = $message;
            }
        }
    }

    public function consume(Transport $transport, int $limit = 0): \Traversable
    {
        return $this->consumeBus->consume($transport, $limit);
    }

    public function stop(): void
    {
        $this->consumeBus->stop();
    }
}
