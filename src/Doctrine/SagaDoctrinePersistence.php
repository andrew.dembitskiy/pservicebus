<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Doctrine;

use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use GDXbsv\PServiceBus\Bus\CoroutineBus;
use GDXbsv\PServiceBus\Id;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBus\Message\MessageHandlerContext;
use GDXbsv\PServiceBus\Saga\Saga;
use GDXbsv\PServiceBus\Saga\SagaFindDefinitions;
use GDXbsv\PServiceBus\Saga\SagaPersistence;
use GDXbsv\PServiceBusTestApp\Saga\TestSaga;

/**
 * @internal
 * @psalm-import-type FindersMap from \GDXbsv\PServiceBus\Saga\ScrapeFinders
 */
final class SagaDoctrinePersistence implements SagaPersistence
{
    private EntityManagerInterface $em;
    private SagaFindDefinitions $sagaFinding;
    private CoroutineBus $coroutineBus;
    /** @var FindersMap */
    private array $findInstructions;
    /** @var array<non-empty-string, object> */
    private array $findToObjectMap;
    private string $tableOutbox;

    /**
     * @param FindersMap $findInstructions
     * @param array<non-empty-string, object> $findToObjectMap
     */
    public function __construct(
        EntityManagerInterface $em,
        SagaFindDefinitions $sagaFinding,
        string $tableOutbox,
        array $findInstructions,
        array $findToObjectMap,
    ) {
        $this->em = $em;
        $this->sagaFinding = $sagaFinding;
        $this->findInstructions = $findInstructions;
        $this->findToObjectMap = $findToObjectMap;
        $this->tableOutbox = $tableOutbox;
    }

    public function retrieveSaga(Message $message, string $sagaType): Saga
    {
        $payload = $message->payload;
        $em = $this->em;
        $em->beginTransaction();
        if (isset($this->findInstructions[$sagaType][$payload::class])) {
            $findInstruction = $this->findInstructions[$sagaType][$payload::class];
            assert(isset($this->findToObjectMap[$findInstruction->className]));
            /**
             * @var Saga $saga
             * @psalm-suppress MixedMethodCall
             */
            $saga = $this->findToObjectMap[$findInstruction->className]->{$findInstruction->methodName}(
                $payload,
                $message->options
            );
            $em->lock($saga, LockMode::PESSIMISTIC_WRITE);

            return $saga;
        }

        foreach ($this->sagaFinding->sagasForMessage($payload) as $finderDefinition) {
            if ($finderDefinition->sagaType !== $sagaType) {
                continue;
            }
            /** @var mixed $propertyValue */
            $propertyValue = ($finderDefinition->messageProperty)(
                $payload,
                new MessageHandlerContext($message->options)
            );

            $qb = $em->createQueryBuilder();

            /** @var Saga|null $saga */
            $saga = $qb
                ->from(TestSaga::class, 'saga')
                ->select('saga')
                ->where($qb->expr()->eq("saga.{$finderDefinition->sagaProperty->getName()}", ':propertyValue'))
                ->setParameter(':propertyValue', $propertyValue)
                ->getQuery()
                ->setLockMode(LockMode::PESSIMISTIC_WRITE)
                ->getOneOrNullResult();
            if ($saga === null) {
                if ($finderDefinition->sagaProperty->getName() === 'id') {
                    $saga = new $finderDefinition->sagaType($propertyValue);
                } else {
                    $saga = new $finderDefinition->sagaType(Id::new());
                }
                $em->persist($saga);
            }

            return $saga;
        }

        throw new \Exception("Did not find saga $sagaType for message " . get_class($message));
    }

    public function saveSaga(Saga $saga): void
    {
        /** @var EntityManager $em */
        $em = $this->em;
        $em->getUnitOfWork()->commit($saga);
        $messages = $saga->getUncommittedMessages();
        $this->saveMessagesInTable($messages);
        $em->commit();
        $coroutine = $this->coroutineBus->publishCoroutine();
        foreach ($messages as $message) {
            $coroutine->send($message);
        }
        $coroutine->send(null);
        $this->deleteMessagesFromTable($messages);
        $em->clear($saga::class);
    }

    public function cleanSaga(Saga $saga): void
    {
        /** @var EntityManager $em */
        $em = $this->em;
        $em->rollback();
        $em->clear();
    }

    public function setCoroutineBus(CoroutineBus $coroutineBus): void
    {
        $this->coroutineBus = $coroutineBus;
    }

    /**
     * @param list<Message<EventOptions>> $messages
     */
    protected function saveMessagesInTable(array $messages): void
    {
        if (count($messages) === 0) {
            return;
        }
        $values = [];
        $parameters = [];
        foreach ($messages as $message) {
            $values[] = "(?, ?)";
            $parameters[] = $message->options->messageId->toString();
            $parameters[] = serialize($message);
        }
        $this->em->getConnection()->executeStatement(
            sprintf(
                "INSERT INTO {$this->tableOutbox} ('message_id', 'message') VALUES  %s",
                join(',', $values)
            ),
            $parameters
        );
    }

    /**
     * @param list<Message<EventOptions>> $messages
     */
    protected function deleteMessagesFromTable(array $messages): void
    {
        if (count($messages) === 0) {
            return;
        }
        $values = [];
        $parameters = [];
        foreach ($messages as $message) {
            $values[] = "?";
            $parameters[] = $message->options->messageId->toString();
        }
        $this->em->getConnection()->executeStatement(
            sprintf(
                "DELETE FROM {$this->tableOutbox} WHERE message_id IN (%s)",
                join(',', $values)
            ),
            $parameters
        );
    }
}
