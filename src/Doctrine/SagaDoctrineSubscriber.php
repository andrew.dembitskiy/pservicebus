<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Doctrine;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use GDXbsv\PServiceBus\Saga\Saga;

class SagaDoctrineSubscriber implements EventSubscriber
{
//    /** @var CoroutineBus */
//    private $coroutineBus;
//
//    public function __construct(CoroutineBus $coroutineBus)
//    {
//        $this->coroutineBus = $coroutineBus;
//    }

    public function getSubscribedEvents()
    {
        return [
            Events::loadClassMetadata,
//            Events::preFlush,
        ];
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs): void
    {
        $classMetadata = $eventArgs->getClassMetadata();
        if (!is_subclass_of($classMetadata->name, Saga::class)) {
            return;
        }
        $fieldMapping = [
            'fieldName' => '_playhead',
            'type' => 'integer',
        ];
        $classMetadata->mapField($fieldMapping);
        $fieldMapping = [
            'id' => true,
            'fieldName' => 'id',
            'type' => 'id',
        ];
        $classMetadata->mapField($fieldMapping);
    }

//    /**
//     * @template T as object
//     */
//    public function preFlush(PreFlushEventArgs $args): void
//    {
//        $unitOfWork = $args->getEntityManager()->getUnitOfWork();
//        /**
//         * @var class-string<T> $class
//         * @psalm-var list<T> $entities
//         */
//        $coroutine = $this->coroutineBus->publishCoroutine();
//        /**
//         * @var string $class
//         * @var list<Saga> $entities
//         */
//        foreach ($unitOfWork->getIdentityMap() as $class => $entities) {
//            if (!is_subclass_of($class, Saga::class)) {
//                continue;
//            }
//            foreach ($entities as $saga) {
//                foreach ($saga->getUncommittedMessages() as $message) {
//                    $coroutine->send($message);
//                }
//            }
//        }
//        $coroutine->send(null);
//    }
}
