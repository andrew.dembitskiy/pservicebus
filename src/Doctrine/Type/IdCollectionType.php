<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\SimpleArrayType;
use GDXbsv\PServiceBus\Id;
use GDXbsv\PServiceBus\IdCollection;

final class IdCollectionType extends SimpleArrayType
{
    const NAME = 'id_collection';


    /**
     * @param string|null      $value
     * @psalm-suppress MoreSpecificImplementedParamType It is okay to have more specific type
     * @return IdCollection
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return IdCollection::ofStrings([]);
        }
        /** @psalm-var non-empty-string[] $ids */
        $ids = parent::convertToPHPValue($value, $platform);

        return IdCollection::ofStrings($ids);
    }

    /**
     * {@inheritdoc}
     *
     * @param IdCollection|null        $value
     * @psalm-suppress MoreSpecificImplementedParamType It is okay to have more specific type
     * @param AbstractPlatform $platform
     *
     * @return string|null
     * @psalm-suppress MixedInferredReturnType we know what parent will return
     * @psalm-suppress MixedReturnStatement we know what parent will return
     *
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        $simpleArray = array_map(
            static function (Id $id) {
                return $id->toString();
            },
            $value->ids
        );

        return parent::convertToDatabaseValue($simpleArray, $platform);
    }

    public function getName()
    {
        return self::NAME;
    }
}
