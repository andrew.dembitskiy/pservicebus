<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus;

use Ramsey\Uuid\Uuid as RamseyUuid;

/**
 * @immutable
 * @psalm-immutable
 * @template CLASS as object
 */
final class Id
{
    /**
     * @var string
     * @psalm-var non-empty-string
     */
    private $value;

    /**
     * @return self
     */
    public static function new(): self
    {
        return new self(RamseyUuid::uuid6()->toString());
    }

    /**
     * @param string $value
     * @psalm-param non-empty-string $value
     */
    public function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @psalm-return non-empty-string
     */
    public function toString(): string
    {
        return $this->value;
    }

    /**
     * @psalm-return non-empty-string
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
