<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class EventOptions extends MessageOptions
{
    protected static string $messageType = 'event';
}
