<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message;

/**
 * @immutable
 * @psalm-immutable
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class ExternalOut
{
    /**
     * @param non-empty-string $transportName
     * @param non-empty-string $externalName
     */
    public function __construct(
        /** @var non-empty-string */
        public string $transportName,
        /** @var non-empty-string */
        public string $externalName
    ) {
    }
}
