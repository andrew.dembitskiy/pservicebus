<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 * @template T as MessageOptions
 */
final class Message
{
    /**
     * @param object $payload
     * @param T $options
     */
    public function __construct(
        public object $payload,
        /** @var T */
        public MessageOptions $options
    ) {
    }
}
