<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class MessageHandlerContext
{
    public function __construct(
        public MessageOptions $messageOptions
    ) {
    }
}
