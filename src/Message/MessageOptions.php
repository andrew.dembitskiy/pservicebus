<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message;

use GDXbsv\PServiceBus\Id;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
class MessageOptions
{
    protected static string $messageType = '';

    private const DATE_FORMAT_STRING = 'Y-m-d\TH:i:s.uP';

    /** @var Id<static> */
    public Id $messageId;

    public \DateTimeImmutable $recordedOn;

    public TimeSpan $timeout;

    public int $retries = 3;

    /** @var array<string, string|int|bool|null> */
    public array $headers = [];

    /**
     * @param Id<static> $messageId
     * @param array<string, string|int|bool|null> $headers
     */
    final private function __construct(Id $messageId, \DateTimeImmutable $recordedOn, array $headers)
    {
        $this->messageId = $messageId;
        $this->recordedOn = $recordedOn;
        $this->headers = $headers;
        $this->headers['message_type'] = static::$messageType;
        $this->timeout = TimeSpan::fromSeconds(0);
    }

    final public static function record(array $headers = []): static
    {
        /** @psalm-suppress MixedArgumentTypeCoercion we know that type is correct */
        return new static(Id::new(), new \DateTimeImmutable(), $headers);
    }

    /**
     * @param array<string, bool|int|string|null> $map
     */
    final public static function fromMap(array $map): static
    {
        assert(
            !empty($map['message_id']) && is_string($map['message_id'])
            && !empty($map['recorded_on']) && is_string($map['recorded_on'])
        );
        $headers = $map;
        unset($headers['message_id'], $headers['recorded_on'], $map['timeout_sec'], $map['retries']);
        $dateTime = \DateTimeImmutable::createFromFormat(self::DATE_FORMAT_STRING, $map['recorded_on']);
        if ($dateTime === false) {
            throw new \RuntimeException("Can not convert to object date: '{$map['recorded_on']}'");
        }
        /** @psalm-suppress ArgumentTypeCoercion we know that type is correct */
        $self = new static(new Id($map['message_id']), $dateTime, $headers);

        /**
         * @psalm-suppress PossiblyInvalidArgument
         */
        $self->timeout = TimeSpan::fromSeconds($map['timeout_sec'] ?? 0);
        if (isset($map['retries']) && is_int($map['retries'])) {
            $self->retries = $map['retries'];
        }

        return $self;
    }

    final public function withHeader(string $name, string|int|bool|null $val): static
    {
        $headers = $this->headers;
        $headers[$name] = $val;
        return new static($this->messageId, $this->recordedOn, $headers);
    }

    final public function withTimeout(TimeSpan $timeSpan): static
    {
        $new = new static($this->messageId, $this->recordedOn, $this->headers);
        $new->timeout = $timeSpan;
        return $new;
    }

    final public function withRetries(int $retries): static
    {
        $new = new static($this->messageId, $this->recordedOn, $this->headers);
        $new->retries = $retries;
        return $new;
    }

    /**
     * @return array<string, string|int|bool|null>
     */
    final public function toMap(): array
    {
        $map = $this->headers;
        $map['message_id'] = $this->messageId->toString();
        $map['recorded_on'] = $this->recordedOn->format(self::DATE_FORMAT_STRING);
        $map['timeout_sec'] = $this->timeout->intervalSec;
        $map['retries'] = $this->retries;

        return $map;
    }
}
