<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message\Replay;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ReplayConsoleCommand extends Command
{
    protected static $defaultName = 'p-service-bus:message:replay';
    private Replaying $replay;

    public function __construct(Replaying $replay)
    {
        $this->replay = $replay;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Replay messages to specific Handlers.')
            ->addUsage('testReplay "\GDXbsv\PServiceBusTestApp\Handling\Handlers::handle2Event1" memory ')
            ->addArgument(
                'replay_name',
                InputArgument::REQUIRED,
                'The replay name. The one from Replay attribute.'
            )
            ->addArgument(
                'handler',
                InputArgument::REQUIRED,
                'A handler name for the message. Usually classFQN::methodName. HANDLER OPTION IS IGNORED when external option is true.'
            )
            ->addArgument(
                'transport',
                InputArgument::REQUIRED,
                'Set the specific transport Name using for replay.'
            )
            ->addOption(
                'external',
                '-e',
                InputOption::VALUE_OPTIONAL,
                'Send to external. THEN HANDLER OPTION IS IGNORED',
                false
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $replayName = $input->getArgument('replay_name');
        $handler = $input->getArgument('handler');
        $transportName = $input->getArgument('transport');
        assert(is_string($transportName) && $transportName !== '');
        $isExternal = (bool)$input->getOption('external');

        if (!is_string($replayName) || empty($replayName)) {
            $output->writeln('<error>Please provide a correct message_name.</error>');

            return self::FAILURE;
        }
        if (!is_string($handler) || empty($handler)) {
            $output->writeln('<error>Please provide a correct handler.</error>');

            return self::FAILURE;
        }

        $progress = new ProgressBar($output);
        $progress->start();
        $progress->setMessage('Replaying ... ');

        $iterator = ($this->replay)($replayName, $handler, $transportName, $isExternal);

        foreach ($iterator as $amount) {
            $progress->advance($amount);
        }

        $progress->finish();

        $output->writeln('Done.');

        return self::SUCCESS;
    }
}
