<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message\Replay;

use GDXbsv\PServiceBus\Message\TimeSpan;

/**
 * @immutable
 * @psalm-immutable
 */
final class ReplayInstruction
{
    public function __construct(
        public string $className,
        public string $methodName,
    ) {
    }
}
