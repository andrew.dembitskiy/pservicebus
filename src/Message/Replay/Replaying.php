<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message\Replay;

use GDXbsv\PServiceBus\Bus\CoroutineBus;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;

/**
 * @psalm-type ReplayOutput = \Traversable<Message<EventOptions>>
 */
class Replaying
{
    private CoroutineBus $coroutineBus;
    /** @var array<non-empty-string, ReplayInstruction> */
    private array $replays;
    /** @var array<non-empty-string, object> */
    private array $replayToObjectMap;

    /**
     * Replaying constructor.
     * @param CoroutineBus $coroutineBus
     * @param array<non-empty-string, ReplayInstruction> $replays
     * @param array<non-empty-string, object> $replayToObjectMap
     */
    public function __construct(CoroutineBus $coroutineBus, array $replays, array $replayToObjectMap)
    {
        $this->coroutineBus = $coroutineBus;
        $this->replays = $replays;
        $this->replayToObjectMap = $replayToObjectMap;
    }

    /**
     * @param non-empty-string $replayName
     * @param non-empty-string $handler
     * @param non-empty-string $transportName
     * @return \Traversable<int, int>
     */
    public function __invoke(
        string $replayName,
        string $handler,
        string $transportName,
        bool $external = false
    ): \Traversable {
        $publisher = $this->coroutineBus->publishCoroutine();
        if (!isset($this->replays[$replayName])) {
            throw new \RuntimeException("Replay with name '$replayName' does not exist.");
        }
        $replayInstruction = $this->replays[$replayName];
        if (!isset($this->replayToObjectMap[$replayInstruction->className])) {
            throw new \RuntimeException("Can not find object for '$replayInstruction->className'.");
        }
        $object = $this->replayToObjectMap[$replayInstruction->className];
        /**
         * @var Message<EventOptions> $message
         * @psalm-suppress MixedMethodCall yes we do not know the object type
         */
        foreach ($object->{$replayInstruction->methodName}() as $message) {
            if (!$external) {
                [$handlerName, $handlerMethodName] = explode('::', $handler);
                /** @psalm-suppress InvalidArgument somehow do not like EventOptions */
                $publisher->send(
                    new Message(
                        $message->payload,
                        $message->options
                            ->withHeader('replay_type', 'internal')
                            ->withHeader('handlerName', $handlerName)
                            ->withHeader('handlerMethodName', $handlerMethodName)
                            ->withHeader('transportName', $transportName)
                    )
                );
            } else {
                /** @psalm-suppress InvalidArgument somehow do not like EventOptions */
                $publisher->send(
                    new Message(
                        $message->payload,
                        $message->options
                            ->withHeader('replay_type', 'external')
                            ->withHeader('transportName', $transportName)
                    )
                );
            }

            yield 1;
        }
        $publisher->send(null);
    }
}
