<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

final class CorrelatedSagaPropertyMapper
{
    private \ReflectionProperty $sagaProperty;
    private SagaMessageFindingConfiguration $sagaMessageFindingConfiguration;
    /** @var class-string<Saga>  */
    private string $sagaType;

    /**
     * @param class-string<Saga> $sagaType
     */
    public function __construct(
        SagaMessageFindingConfiguration $sagaMessageFindingConfiguration,
        string $sagaType,
        \ReflectionProperty $sagaProperty
    ) {
        $this->sagaMessageFindingConfiguration = $sagaMessageFindingConfiguration;
        $this->sagaProperty                    = $sagaProperty;
        $this->sagaType                        = $sagaType;
    }

    /**
     * @param \Closure(object):\GDXbsv\PServiceBus\Id<Saga> $messageProperty
     */
    public function toMessage(\Closure $messageProperty): self
    {
        $this->sagaMessageFindingConfiguration->configureMapping($this->sagaType, $this->sagaProperty, $messageProperty);

        return $this;
    }
}
