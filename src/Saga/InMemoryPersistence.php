<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

use GDXbsv\PServiceBus\Bus\CoroutineBus;
use GDXbsv\PServiceBus\Id;
use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBus\Message\MessageHandlerContext;

/**
 * @internal
 * @psalm-import-type FindersMap from \GDXbsv\PServiceBus\Saga\ScrapeFinders
 */
final class InMemoryPersistence implements SagaPersistence
{
    private SagaFindDefinitions $sagaFinding;
    private CoroutineBus $coroutineBus;
    /** @var array<string, Saga> */
    private array $sagaList = [];
    /** @var FindersMap */
    private array $findInstructions;
    /** @var array<non-empty-string, object> */
    private array $findToObjectMap;

    /**
     * @param FindersMap $findInstructions
     * @param array<non-empty-string, object> $findToObjectMap
     */
    public function __construct(
        SagaFindDefinitions $sagaFinding,
        array $findInstructions,
        array $findToObjectMap,
    ) {
        $this->sagaFinding = $sagaFinding;
        $this->findInstructions = $findInstructions;
        $this->findToObjectMap = $findToObjectMap;
    }

    public function setCoroutineBus(CoroutineBus $coroutineBus): void
    {
        $this->coroutineBus = $coroutineBus;
    }

    public function retrieveSaga(Message $message, string $sagaType): Saga
    {
        $payload = $message->payload;
        if (isset($this->findInstructions[$sagaType][$payload::class])) {
            $findInstruction = $this->findInstructions[$sagaType][$payload::class];
            assert(isset($this->findToObjectMap[$findInstruction->className]));
            /**
             * @var Saga $saga
             * @psalm-suppress MixedMethodCall
             */
            $saga = $this->findToObjectMap[$findInstruction->className]->{$findInstruction->methodName}(
                $payload,
                $message->options
            );
            return $saga;
        }

        foreach ($this->sagaFinding->sagasForMessage($payload) as $finderDefinition) {
            if ($finderDefinition->sagaType !== $sagaType) {
                continue;
            }
            assert(
                $finderDefinition->sagaProperty->name === 'id',
                "Sorry for in memory we support only mapping to ID. '{$finderDefinition->sagaProperty->name}'"
            );
            /** @var Id<Saga> $id */
            $id = ($finderDefinition->messageProperty)($payload, new MessageHandlerContext($message->options));

            if (isset($this->sagaList[$id->toString()])) {
                $saga = clone($this->sagaList[$id->toString()]);
            } else {
                $saga = new $finderDefinition->sagaType($id);
            }

            return $saga;
        }

        throw new \Exception("Did not find saga $sagaType for message " . get_class($message->payload));
    }

    public function saveSaga(Saga $saga): void
    {
        [$id,] = $this->getSagaInfo($saga);
        $coroutine = $this->coroutineBus->publishCoroutine();
        foreach ($saga->getUncommittedMessages() as $message) {
            $coroutine->send($message);
        }
        $coroutine->send(null);
        $this->sagaList[$id->toString()] = $saga;
    }

    public function cleanSaga(Saga $saga): void
    {
    }

    /**
     * @return array{Id<Saga>, positive-int}
     * @psalm-suppress MixedInferredReturnType
     */
    protected function getSagaInfo(Saga $saga): array
    {
        /** @var \Closure():array{Id<Saga>, positive-int} $closure */
        $closure = function (): array {
            /**
             * @var Saga $this
             * @psalm-suppress InaccessibleProperty
             */
            return [$this->id, $this->_playhead];
        };
        /** @psalm-suppress MixedReturnStatement */
        return $closure->call($saga);
    }
}
