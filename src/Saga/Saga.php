<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

use GDXbsv\PServiceBus\Id;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBus\Message\TimeSpan;

abstract class Saga
{
    /** @var Id<static> */
    protected Id $id;
    /** @var list<Message<EventOptions>> */
    private array $uncommittedMessages = [];
    /** @var int */
    protected int $_playhead = 0;

    /**
     * @param Id<static> $id
     */
    final public function __construct(Id $id)
    {
        $this->id = $id;
    }

    abstract public static function configureHowToFindSaga(SagaPropertyMapper $mapper): void;

    /**
     * @throws \Exception
     */
    protected function apply(object $event): void
    {
        ++$this->_playhead;
        /** @psalm-suppress InvalidPropertyAssignmentValue psalm does not detect correct type */
        $this->uncommittedMessages[] = new Message(
            $event,
            EventOptions::record()->withHeader('playhead', $this->_playhead)
        );
    }

    /**
     * @throws \Exception
     */
    protected function timeout(object $event, TimeSpan $timeSpan): void
    {
        ++$this->_playhead;
        /** @psalm-suppress InvalidPropertyAssignmentValue psalm does not detect correct type */
        $this->uncommittedMessages[] = new Message(
            $event,
            EventOptions::record()->withHeader(
                'playhead',
                $this->_playhead
            )->withTimeout($timeSpan)
        );
    }

    /**
     * @return list<Message<EventOptions>>
     */
    public function getUncommittedMessages(): array
    {
        $collected = $this->uncommittedMessages;
        $this->uncommittedMessages = [];

        return $collected;
    }
}
