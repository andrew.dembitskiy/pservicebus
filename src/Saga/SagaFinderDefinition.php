<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

final class SagaFinderDefinition
{
    /** @var class-string<Saga> */
    public string $sagaType;
    /** @var \ReflectionProperty */
    public \ReflectionProperty $sagaProperty;
    /** @param \Closure(object):\GDXbsv\PServiceBus\Id<Saga> $messageProperty */
    public \Closure $messageProperty;

    /**
     * @param class-string<Saga>        $sagaType
     * @param \ReflectionProperty $sagaProperty
     * @param \Closure(object):\GDXbsv\PServiceBus\Id<Saga>   $messageProperty
     */
    public function __construct(string $sagaType, \ReflectionProperty $sagaProperty, \Closure $messageProperty)
    {
        $this->sagaType        = $sagaType;
        $this->sagaProperty    = $sagaProperty;
        $this->messageProperty = $messageProperty;
    }
}
