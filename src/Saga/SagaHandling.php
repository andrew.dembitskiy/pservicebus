<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBus\Message\MessageHandlerContext;

/**
 * @internal
 */
final class SagaHandling
{
    private SagaPersistence $sagaPersistence;

    public function __construct(
        SagaPersistence $sagaPersistence,
    ) {
        $this->sagaPersistence = $sagaPersistence;
    }

    public function handle(Message $message): void
    {
        try {
            $saga = $this->execute($message);
            $this->sagaPersistence->saveSaga($saga);
        } catch (\Throwable $throwable) {
            if (isset($saga)) {
                $this->sagaPersistence->cleanSaga($saga);
            }
            throw $throwable;
        }
    }

    protected function execute(Message $message): Saga
    {
        assert(isset($message->options->headers['saga']));
        /** @var class-string<Saga> $sagaType */
        $sagaType = $message->options->headers['saga'];
        $messagePayload = $message->payload;
        $saga = $this->sagaPersistence->retrieveSaga($message, $sagaType);
        $sagaReflection = new \ReflectionClass($saga);
        foreach (
            $sagaReflection->getMethods(
                \ReflectionMethod::IS_STATIC | \ReflectionMethod::IS_PUBLIC
            ) as $reflectionMethod
        ) {
            $parameters = $reflectionMethod->getParameters();
            if (count($parameters) !== 2) {
                continue;
            }
            [$messageParameter, ,] = $parameters;
            /** @var \ReflectionNamedType|null $messageType */
            $messageType = $messageParameter->getType();
            if ($messageType && $messageType->getName() !== get_class($messagePayload)) {
                continue;
            }
            $reflectionMethod->invokeArgs(
                $saga,
                [
                    $messagePayload,
                    new MessageHandlerContext($message->options),
                ]
            );
        }

        return $saga;
    }
}
