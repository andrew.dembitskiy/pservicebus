<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

final class SagaMapper implements SagaMessageFindingConfiguration, SagaFindDefinitions
{
    /** @var array<class-string, array<string, SagaToMessageMap>> */
    private array $mappings = [];

    /**
     * @param list<class-string<Saga>> $sagasClasses
     */
    public function __construct(array $sagasClasses)
    {
        $this->scanSagas($sagasClasses);
    }

    public function configureMapping(
        string $sagaType,
        \ReflectionProperty $sagaProperty,
        \Closure $messageProperty
    ): void {
        $reflection = new \ReflectionFunction($messageProperty);
        $arguments  = $reflection->getParameters();
        assert(isset($arguments[0]));
        /** @var \ReflectionNamedType $type */
        $type = $arguments[0]->getType();
        /** @var class-string $messageType */
        $messageType = $type->getName();

        $this->mappings[$messageType][$sagaType] = new PropertyFinderSagaToMessageMap($sagaType, $sagaProperty, $messageProperty);
    }

    public function sagasForMessage(object $message): \Traversable
    {
        $finders = $this->mappings[get_class($message)] ?? [];
        if (count($finders) === 0) {
            return;
        }

        foreach ($finders as $finder) {
            yield $finder->createSagaFinderDefinition();
        }
    }

    /**
     * @param list<class-string<Saga>> $sagasClasses
     */
    private function scanSagas(array $sagasClasses): void
    {
        foreach ($sagasClasses as $sagaClass) {
            $sagaPropertyMapper = new SagaPropertyMapper($sagaClass, $this);
            $sagaClass::configureHowToFindSaga($sagaPropertyMapper);
        }
    }
}
