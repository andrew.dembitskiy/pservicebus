<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

interface SagaMessageFindingConfiguration
{
    /**
     * @param \ReflectionProperty $sagaProperty
     * @param \Closure(object):\GDXbsv\PServiceBus\Id<Saga> $messageProperty
     * @param class-string<Saga>  $sagaType
     */
    public function configureMapping(
        string $sagaType,
        \ReflectionProperty $sagaProperty,
        \Closure $messageProperty
    ): void;
}
