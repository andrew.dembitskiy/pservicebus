<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

use GDXbsv\PServiceBus\Message\Message;

interface SagaPersistence
{
    /**
     * @param class-string<Saga> $sagaType
     */
    public function retrieveSaga(Message $message, string $sagaType): Saga;

    public function saveSaga(Saga $saga): void;

    public function cleanSaga(Saga $saga): void;
}
