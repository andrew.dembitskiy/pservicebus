<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

final class SagaPropertyMapper
{
    private SagaMessageFindingConfiguration $sagaMessageFindingConfiguration;
    /** @var class-string<Saga>  */
    private string $sagaType;

    /**
     * @param class-string<Saga> $sagaType
     */
    public function __construct(string $sagaType, SagaMessageFindingConfiguration $sagaMessageFindingConfiguration)
    {
        $this->sagaMessageFindingConfiguration = $sagaMessageFindingConfiguration;
        $this->sagaType = $sagaType;
    }

    public function mapSaga(\ReflectionProperty $sagaProperty): CorrelatedSagaPropertyMapper
    {
        return new CorrelatedSagaPropertyMapper($this->sagaMessageFindingConfiguration, $this->sagaType, $sagaProperty);
    }
}
