<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Serializer;

use Doctrine\Instantiator\Instantiator;
use GDXbsv\PServiceBus\Id;
use GDXbsv\PServiceBus\IdCollection;

class ClojureSerializer implements Serializer
{
    /**
     * @psalm-suppress MixedInferredReturnType
     */
    public function serialize(object $data): array
    {
        $hydrate = \Closure::bind(
            static function (object $object): array {
                /** @var array<scalar|object> $vars */
                $vars = get_object_vars($object);
                foreach ($vars as &$var) {
                    if (\is_object($var)) {
                        if ($var instanceof Id) {
                            $var = $var->toString();
                            continue;
                        }
                        if ($var instanceof IdCollection) {
                            $var = array_map(fn(Id $id) => $id->toString(), $var->ids);
                            continue;
                        }
                        if ($var instanceof \DateTimeInterface) {
                            $var = $var->format('Y-m-d\TH:i:s.uP');
                            continue;
                        }
                        throw new \Exception(
                            'You can serealize objects, you gave '.get_class($var)
                        );
                    }
                }

                return $vars;
            },
            null,
            get_class($data)
        );

        /**
         * @psalm-suppress PossiblyInvalidFunctionCall
         * @psalm-suppress MixedReturnStatement
         */
        return $hydrate($data);
    }

    /**
     * @param array{properties: mixed} $serializedData
     * @return object
     * @throws \Exception
     * @psalm-suppress MoreSpecificImplementedParamType
     */
    public function deserialize(array $serializedData, string $class): object
    {
        /**
         * @var self $instance
         * @var class-string $class
         */
        $instance = (new Instantiator())->instantiate($class);
        $hydrate  = \Closure::bind(
            function (array $payload, object $object): void {
                /** @var array<string, scalar|array<scalar>> $payload */
                foreach ($payload as $var => $value) {
                    $classReflection = new \ReflectionClass($object);
                    if (!$classReflection->hasProperty($var)) {
                        continue;
                    }
                    $property = $classReflection->getProperty($var);
                    if (!$property->hasType()) {
                        continue;
                    }
                    /** @var \ReflectionNamedType $propertyType */
                    $propertyType = $property->getType();
                    if (class_exists($class = $propertyType->getName())) {
                        if ($class === IdCollection::class) {
                            /**
                             * @psalm-suppress InvalidScalarArgument we know that it should be an array
                             */
                            $object->{$var} = IdCollection::ofStrings($value);
                            continue;
                        }
                        /** @psalm-suppress MixedMethodCall */
                        $object->{$var} = new $class($value);
                        continue;
                    }
                    $object->{$var} = $value;
                }
            },
            null,
            $class
        );
        /** @psalm-suppress PossiblyInvalidFunctionCall */
        $hydrate($serializedData, $instance);

        return $instance;
    }
}
