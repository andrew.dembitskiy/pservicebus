<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Serializer;

interface Serializer
{
    /**
     * @param object $data
     * @return array
     */
    public function serialize(object $data): array;

    /**
     * @param array $serializedData
     * @return object
     */
    public function deserialize(array $serializedData, string $class): object;
}
