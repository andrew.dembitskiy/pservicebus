<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport\Bunny;

use Bunny\Message;
use GDXbsv\PServiceBus\Transport\Envelope;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class BunnyEnvelope
{
    /**
     * @param string $payload
     * @param int $retries
     * @param int $timeoutSec
     * @param array<string, string|int|bool|null> $headers
     */
    public function __construct(
        private string $payload,
        private int $retries,
        private int $timeoutSec,
        /** @var array<string, string|int|bool|null> */
        private array $headers,
    ) {
    }

    public static function ofEnvelope(Envelope $envelope): self
    {
        return new self(
            \json_encode($envelope->payload, JSON_THROW_ON_ERROR),
            $envelope->retries,
            $envelope->timeoutSec,
            $envelope->headers
        );
    }

    public static function ofBunnyMessage(Message $message): self
    {
        $reties = $message->headers['retries'] ?? 0;
        $timeoutSec = $message->headers['timeout_sec'] ?? 0;
        /** @psalm-suppress MixedArgumentTypeCoercion */
        return new self($message->content, $reties, $timeoutSec, $message->headers);
    }

    public function toBunnyPublish(): array
    {
        $headers = $this->headers;
        $headers['retries'] = $this->retries;
        $headers['timeout_sec'] = $this->timeoutSec;
        $headers['psb-retried'] = 0;
        if ($this->timeoutSec > 0) {
            $headers['expiration'] = $this->timeoutSec * 1000;
        }

        return [$this->payload, $headers];
    }

    public function toEnvelope(): Envelope
    {
        /** @psalm-suppress MixedArgument we control what we receive. It must be an array */
        return new Envelope(\json_decode($this->payload, true), $this->retries, $this->timeoutSec, $this->headers);
    }
}
