<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport\Bunny;

use Bunny\Channel;
use GDXbsv\PServiceBus\Transport\Envelope;
use React\EventLoop\LoopInterface;
use React\Promise\PromiseInterface;

use function Clue\React\Block\await;
use function React\Promise\all;

/**
 * @internal
 * WARNING!! With current inplementation it can be only one external queue
 * @psalm-import-type MessageNameMap from \GDXbsv\PServiceBus\Message\ScrapeExternals
 */
final class BunnyTransportExternal extends BunnyTransportInternal
{
    public function __construct(
        private string $queueName,
        protected PromiseInterface $channel,
        protected LoopInterface $loop,
        /** @var MessageNameMap */
        private array $messageNameMapOut,
        /** @var MessageNameMap */
        private array $messageNameMapIn,
    ) {
        parent::__construct($queueName, $channel, $loop);
    }

    protected function sendEnvelope(Envelope $envelope): PromiseInterface
    {
        return $this->channel
            ->then(
                function (Channel $channel) use ($envelope) {
                    [$body, $headers] = BunnyEnvelope::ofEnvelope($envelope)->toBunnyPublish();
                    if (!isset($this->messageNameMapOut[$envelope->headers['name']])) {
                        throw new \RuntimeException('You can send only ExternalOut messages to external transport.');
                    }
                    return $channel->publish($body, $headers, exchange: $envelope->headers['name']);
                }
            );
    }

    public function sync(): void
    {
        parent::sync();
        await(
            $this->channel
                ->then(
                    function (Channel $channel) {
                        $promises = [];
                        foreach (array_keys($this->messageNameMapOut) as $messageExternalName) {
                            $promises[] = $channel->exchangeDeclare($messageExternalName, 'fanout');
                        }
                        foreach (array_keys($this->messageNameMapIn) as $messageExternalName) {
                            // WARNING!! With current inplementation it can be only one external queue. We subscribe all to this specific queue
                            $promises[] = $channel->queueBind($this->queueName, $messageExternalName);
                        }
                        return all($promises);
                    }
                ),
            $this->loop
        );
    }
}
