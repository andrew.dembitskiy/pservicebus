<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport;

use GDXbsv\PServiceBus\Bus\ConsumeBus;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Webmozart\Assert\Assert;

/**
 * @psalm-type TRANSPORTS=array<string, Transport>
 */
class ConsumeConsoleCommand extends Command
{
    protected static $defaultName = 'p-service-bus:transport:consume';

    /** @var TRANSPORTS */
    private $transports;
    private ConsumeBus $consumeBus;

    /**
     * @param TRANSPORTS $transports
     */
    public function __construct(ConsumeBus $consumeBus, array $transports)
    {
        $this->transports = $transports;
        $this->consumeBus = $consumeBus;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument(
                'transport',
                InputArgument::OPTIONAL,
                'Name of the transport.'
            )
            ->addOption(
                'limit',
                'l',
                InputOption::VALUE_REQUIRED,
                'How many queued items should be processed. 0 means infinity.)',
                '0'
            )
            ->setDescription('Get items from the queue and project them.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $limit = (int)$input->getOption('limit');
        if ($limit === 0) {
            $io->warning('Limit is "0". The command will not stop to work.');
        }

        $transport = $this->receiveTransportBus($input, $io);

        \pcntl_signal(\SIGINT, fn(int $_signo) => $this->consumeBus->stop());
        \pcntl_signal(\SIGTERM, fn(int $_signo)  => $this->consumeBus->stop());

        foreach ($this->consumeBus->consume($transport, $limit) as $_domainMessage) {
            pcntl_signal_dispatch();
        }

        $output->writeln('<info>Succeed</info>');

        return 0;
    }

    /**
     * @return Transport
     */
    private function receiveTransportBus(InputInterface $input, SymfonyStyle $io): Transport
    {
        $busService = $input->getArgument('transport');
        if (!is_string($busService)) {
            $question = new ChoiceQuestion('Please select a transport.', array_keys($this->transports),);
            $question->setErrorMessage('Transport %s is invalid.');

            $busService = (string)$io->askQuestion($question);
        }

        Assert::keyExists($this->transports, $busService);

        return $this->transports[$busService];
    }
}
