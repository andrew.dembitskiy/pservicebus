<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class Envelope
{
    /**
     * @param array $payload
     * @param int $retries
     * @param int $timeoutSec
     * @param array<string, string|int|bool|null> $headers
     */
    public function __construct(
        public array $payload,
        public int $retries,
        public int $timeoutSec,
        /** @var array<string, string|int|bool|null> */
        public array $headers,
    ) {
    }
}
