<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport\Sns;

use GDXbsv\PServiceBus\Transport\Envelope;

/**
 * @internal
 */
final class SnsEnvelope
{
    /**
     * @param string $payload
     * @param int $retries
     * @param int $timeoutSec
     * @param array<string, string|int|bool|null> $headers
     */
    public function __construct(
        private string $payload,
        private int $retries,
        private int $timeoutSec,
        /** @var array<string, string|int|bool|null> */
        private array $headers,
    ) {
    }

    public static function ofEnvelope(Envelope $envelope): self
    {
        return new self(
            \json_encode($envelope->payload, JSON_THROW_ON_ERROR),
            $envelope->retries,
            $envelope->timeoutSec,
            $envelope->headers
        );
    }

    public function toSnsArray(string $topicArn): array
    {
        $messageAttributes = $this->generateAttributes($this->headers);

        $publish = [
            'Message'  => $this->payload,
            'TopicArn' => $topicArn,
        ];

        if (0 !== count($messageAttributes)) {
            $publish['MessageAttributes'] = $messageAttributes;
        }

        return $publish;
    }

    public function toEnvelope(): Envelope
    {
        /** @psalm-suppress MixedArgument we control what we receive. It must be an array */
        return new Envelope(\json_decode($this->payload, true), $this->retries, $this->timeoutSec, $this->headers);
    }

    /**
     * @param iterable<string,string|int|bool|null> $attributes
     * @return array
     */
    private function generateAttributes(iterable $attributes): array
    {
        $messageAttributes = [];
        foreach ($attributes as $attributeName => $attributeValue) {
            $dataType = 'String';
            if (is_numeric($attributeValue)) {
                $dataType = 'Number';
            }
            $messageAttributes[$attributeName] = [
                'DataType'    => $dataType,
                'StringValue' => $attributeValue,
            ];
        }

        return $messageAttributes;
    }
}
