<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport\Sns;

use Aws\Sns\SnsClient;
use Enqueue\Dsn\Dsn;
use GDXbsv\PServiceBus\Transport\Envelope;
use GDXbsv\PServiceBus\Transport\Sqs\SqsEnvelope;
use GDXbsv\PServiceBus\Transport\Sqs\SqsTransport;
use GDXbsv\PServiceBus\Transport\Transport;
use GDXbsv\PServiceBus\Transport\TransportSynchronisation;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Promise\Utils;
use Prewk\Result;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Now support only one queue for all messages.
 * You can modify this transport for multi sqs queues and multi topics support.
 * For example topic+sqs for each message, ot subscription+sqs for each message
 *
 * @psalm-import-type MessageNameMap from \GDXbsv\PServiceBus\Message\ScrapeExternals
 */
class SnsSqsTransport implements TransportSynchronisation, Transport
{
    private const PART_ONE = 0;
    private const PART_TWO = 1;
    private const ACCUMULATE = 30;

    private bool $shouldStop = false;

    public function __construct(
        private SnsClient $snsClient,
        private string $topicArn,
        private SqsTransport $sqsTransport,
        private LoggerInterface $logger,
        /** @var MessageNameMap */
        private array $messageNameMapOut,
        /** @var MessageNameMap */
        private array $messageNameMapIn,
    ) {
        $this->sqsTransport->setAllowFrom($this->topicArn);
    }

    /**
     * @param string $dsnString
     * @param SqsTransport $sqsTransport
     * @param MessageNameMap $messageNameMapOut
     * @param MessageNameMap $messageNameMapIn
     * @param array $clientAdditionalConfig
     * @param LoggerInterface|null $logger
     * @return self
     */
    public static function ofDsn(
        string $dsnString,
        SqsTransport $sqsTransport,
        /** @var MessageNameMap */
        array $messageNameMapOut,
        /** @var MessageNameMap */
        array $messageNameMapIn,
        array $clientAdditionalConfig = [],
        ?LoggerInterface $logger
    ): self {
        $dsn = Dsn::parseFirst($dsnString);
        if (null === $dsn || $dsn->getSchemeProtocol() !== 'sns') {
            throw new \InvalidArgumentException(
                'Malformed parameter "dsn". example: "sns+http://key:secret@aws:4100/123456789012?region=eu-west-1&topic=testTopic"'
            );
        }

        $namespace = $dsn->getPath();
        assert(
            $namespace !== null,
            'error(-><-): sns+http://key:secret@aws:4100->/123456789012<-?region=eu-west-1&topic=testTopic'
        );
        $topic = $dsn->getString('topic');
        assert(
            $topic !== null,
            'error(-><-): sns+http://key:secret@aws:4100/123456789012?region=eu-west-1&->topic=testTopic<-'
        );
        $region = $dsn->getString('region');
        assert(
            $region !== null,
            'error(-><-): sns+http://key:secret@aws:4100/123456789012?->region=eu-west-1<-&topic=testTopic'
        );
        $user = $dsn->getUser();
        assert(
            $user !== null,
            'error(-><-): sns+http://->key<-:secret@aws:4100/123456789012?region=eu-west-1&topic=testTopic'
        );
        $password = $dsn->getPassword();
        assert(
            $password !== null,
            'error(-><-): sns+http://key:->secret<-@aws:4100/123456789012?region=eu-west-1&topic=testTopic'
        );

        $topicArn = 'arn:aws:sns:' . $region . ':' . trim($namespace, '/') . ':' . $topic;

        $snsClientConfig = [
            'version' => 'latest',
            'region' => $region,
            'credentials' => [
                'key' => $user,
                'secret' => $password,
            ],
        ];
        $host = $dsn->getHost();
        if ($host !== null) {
            /**
             * @psalm-suppress PossiblyUndefinedIntArrayOffset
             * @var string $schemeExtension
             */
            $schemeExtension = $dsn->getSchemeExtensions()[0];
            $endpoint = sprintf(
                '%s://%s',
                $schemeExtension,
                $host
            );
            if ($dsn->getPort() !== null) {
                $endpoint .= ":{$dsn->getPort()}";
            }
            $snsClientConfig['endpoint'] = $endpoint;
        }
        return new self(
            new SnsClient(array_merge_recursive($snsClientConfig, $clientAdditionalConfig)),
            $topicArn,
            $sqsTransport,
            $logger ?? new NullLogger(),
            $messageNameMapOut,
            $messageNameMapIn,
        );
    }

    /**
     * @return \Generator<int, Envelope, Result\Ok<null, mixed>|Result\Err<mixed, \Exception>, void>
     */
    public function receive(int $limit = 0): \Generator
    {
        $envelopesGenerator = $this->sqsTransport->receive($limit);
        while ($envelopesGenerator->valid()) {
            $envelope = $envelopesGenerator->current();
            /** @var array{Type:string, Message: string, MessageId:string, TopicArn:string, Timestamp:string, MessageAttributes: array<string, array{Type:string, Value:string}>} $message */
            $message = $envelope->payload;
            $result = yield SqsEnvelope::ofSqs(
                [
                    'Body' => $message['Message'],
                    'Attributes' => $this->headersFromAttributes($message['MessageAttributes'])
                ]
            )->toEnvelope();
            $envelopesGenerator->send($result);

            if ($this->shouldStop) {
                $this->sqsTransport->stop();
            }
        }
    }

    public function stop(): void
    {
        $this->shouldStop = true;
    }

    public function sync(): void
    {
        $this->sqsTransport->sync();
        $this->subscribeSqs($this->sqsTransport->arn());
    }

    /**
     * @return \Generator<int, void, Envelope|null, void>
     * @psalm-suppress MixedReturnTypeCoercion
     */
    public function sending(): \Generator
    {
        $envelope = (yield);
        if ($envelope === null) {
            return;
        }
        $promises = [
            self::PART_ONE => [],
            self::PART_TWO => [],
        ];
        $partCurrent = self::PART_ONE;
        while ($envelope) {
            $promises[$partCurrent][] = $this->publishMessage($envelope);
            /** @psalm-suppress RedundantCondition */
            if ($partCurrent === self::PART_ONE && count($promises[self::PART_ONE]) >= 15) {
                $partCurrent = self::PART_TWO;
                if (count($promises[self::PART_TWO]) >= 15) {
                    Utils::all($promises[self::PART_TWO])->wait();
                    $promises[self::PART_TWO] = [];
                }
            }
            if ($partCurrent === self::PART_TWO && count($promises[self::PART_TWO]) >= 15) {
                $partCurrent = self::PART_ONE;
                if (count($promises[self::PART_ONE]) >= 15) {
                    Utils::all($promises[self::PART_ONE])->wait();
                    $promises[self::PART_ONE] = [];
                }
            }
            $envelope = (yield);
        }

        Utils::all(\array_merge($promises[self::PART_TWO], $promises[self::PART_ONE]))->wait();

        $promises[self::PART_TWO] = [];
        $promises[self::PART_ONE] = [];
    }

    private function publishMessage(Envelope $envelope): PromiseInterface
    {
        if (!isset($this->messageNameMapOut[$envelope->headers['name']])) {
            throw new \RuntimeException('You can send only ExternalOut messages to external transport.');
        }
        $publish = (SnsEnvelope::ofEnvelope($envelope))->toSnsArray($this->topicArn);
        $promise = $this->snsClient->publishAsync($publish)->then(
            null,
            function (\Throwable $error) use ($publish): void {
                $this->logger->error(
                    'Could not send messages to the sns.',
                    [
                        'exception' => $error,
                        'message' => $publish,
                    ]
                );

                throw $error;
            }
        );

        Utils::queue()->run();

        return $promise;
    }

    /**
     * @param array<string, array{Type:string, Value:string}> $attributes
     * @return array<string,string>
     */
    private function headersFromAttributes(array $attributes): array
    {
        $attr = [];
        foreach ($attributes as $attributeName => $attributeBody) {
            $attr[$attributeName] = $attributeBody['Value'];
        }
        return $attr;
    }

    private function subscribeSqs(string $sqsArn): void
    {
        $this->subscribe('sqs', $sqsArn);
    }

    private function subscribe(string $protocol, string $endpoint): ?string
    {
        $filterEvents = array_keys($this->messageNameMapIn);
        $subscriptionArn = $this->subscriptionArn($endpoint);

        if (null !== $subscriptionArn && 0 === count($filterEvents)) {
            $this->unsubscribe($subscriptionArn);

            return null;
        }

        if (null === $subscriptionArn && 0 === count($filterEvents)) {
            return null;
        }

        if (null === $subscriptionArn) {
            $subscriptionResult = $this->snsClient->subscribe(
                [
                    'Protocol' => $protocol,
                    'Endpoint' => $endpoint,
                    'ReturnSubscriptionArn' => true,
                    'TopicArn' => $this->topicArn,
                ]
            );
            /** @var string $subscriptionArn */
            $subscriptionArn = $subscriptionResult->get('SubscriptionArn');
        }

        $this->snsClient->setSubscriptionAttributes(
            [
                'SubscriptionArn' => $subscriptionArn,
                'AttributeName' => 'FilterPolicy',
                'AttributeValue' => json_encode(['name' => $filterEvents], JSON_THROW_ON_ERROR),
            ]
        );

        return $subscriptionArn;
    }

    private function subscriptionArn(string $endpoint): ?string
    {
        $params = ['TopicArn' => $this->topicArn,];
        while (true) {
            /** @var array{NextToken:string, Subscriptions:array<array{Endpoint: string, SubscriptionArn:string}>} $result */
            $result = $this->snsClient->listSubscriptionsByTopic($params);

            foreach ($result['Subscriptions'] ?? [] as $subscription) {
                if ($endpoint === $subscription['Endpoint']) {
                    return $subscription['SubscriptionArn'];
                }
            }

            if (isset($result['NextToken'])) {
                $params['NextToken'] = $result['NextToken'];
            } else {
                return null;
            }
        }

        return null;
    }

    private function unsubscribe(string $subscriptionArn): void
    {
        $this->snsClient->unsubscribe(
            [
                'SubscriptionArn' => $subscriptionArn,
            ]
        );
    }
}
