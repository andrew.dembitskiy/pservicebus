<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport\Sqs;

use GDXbsv\PServiceBus\Transport\Envelope;
use Ramsey\Uuid\Uuid;

/**
 * @internal
 */
final class SqsEnvelope
{
    /**
     * @param string $payload
     * @param int $retries
     * @param int $timeoutSec
     * @param array<string, string|int|bool|null> $headers
     */
    public function __construct(
        private string $payload,
        private int $retries,
        private int $timeoutSec,
        /** @var array<string, string|int|bool|null> */
        private array $headers,
    ) {
    }

    public static function ofEnvelope(Envelope $envelope): self
    {
        return new self(
            \json_encode($envelope->payload, JSON_THROW_ON_ERROR),
            $envelope->retries,
            $envelope->timeoutSec,
            $envelope->headers
        );
    }

    /**
     * @param array{Body:string, Attributes:array<string,string|int>} $message
     */
    public static function ofSqs(array $message): self
    {
        $reties = $message['Attributes']['retries'] ?? 0;
        $timeoutSec = $message['Attributes']['timeout_sec'] ?? 0;
        return new self($message['Body'], (int)$reties, (int)$timeoutSec, $message['Attributes']);
    }

    public function toSqsArray(): array
    {
        $messageAttributes = $this->generateAttributes($this->headers);
        if ($this->timeoutSec < 0 || $this->timeoutSec > 900) {
            throw new \Exception('Sorry but we support 0-900 sec timeout for SQS only;');
        }
        $message = [
            'MessageBody' => $this->payload,
            'Id' => Uuid::uuid6(),
            'DelaySeconds' => $this->timeoutSec,
        ];
        if (0 !== count($messageAttributes)) {
            $message['MessageAttributes'] = $messageAttributes;
        }

        return $message;
    }

    public function toEnvelope(): Envelope
    {
        /** @psalm-suppress MixedArgument we control what we receive. It must be an array */
        return new Envelope(\json_decode($this->payload, true), $this->retries, $this->timeoutSec, $this->headers);
    }

    /**
     * @param iterable<string,bool|int|null|string> $attributes
     * @return array
     */
    private function generateAttributes(iterable $attributes): array
    {
        $messageAttributes = [];
        foreach ($attributes as $attributeName => $attributeValue) {
            $dataType = 'String';
            if (is_numeric($attributeValue)) {
                $dataType = 'Number';
            }
            $messageAttributes[$attributeName] = [
                'DataType' => $dataType,
                'StringValue' => $attributeValue,
            ];
        }

        return $messageAttributes;
    }
}
