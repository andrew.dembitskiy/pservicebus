<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TransportSyncConsoleCommand extends Command
{
    protected static $defaultName = 'p-service-bus:transport:sync';
    /**
     * @var TransportSynchronisation[]
     */
    private $transportSynchronisations;

    /**
     * @param TransportSynchronisation[] $transportSynchronisations
     */
    public function __construct(array $transportSynchronisations)
    {
        parent::__construct();
        $this->transportSynchronisations = $transportSynchronisations;
    }

    protected function configure(): void
    {
        $this->setDescription('Make sure all necessary resources for your transports are created and configured.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Sync transport...');
        foreach ($this->transportSynchronisations as $transportSynchronisation) {
            $transportSynchronisation->sync();
        }
        $output->writeln('Sync done.');

        return 0;
    }
}
