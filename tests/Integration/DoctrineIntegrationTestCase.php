<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusTests\Integration;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBus\Setup;
use GDXbsv\PServiceBusTestApp\Saga\CustomDoctrineSagaFinder;
use Symfony\Component\Console\Tester\CommandTester;

class DoctrineIntegrationTestCase extends IntegrationTestCase
{
    protected EntityManager $em;

    protected function setUp(): void
    {
        parent::setUp();
        $this->prepareDb();
    }

    protected function prepareSetup(): Setup
    {
        $setup = parent::prepareSetup();
        $setup->addDoctrine([__DIR__ . "/../../TestApp/Saga"], 'messages_to_send', 'only_once');
        $this->em = $setup->getEntityManager();
        $setup->addSagaFinders(
            [CustomDoctrineSagaFinder::class],
            [
                CustomDoctrineSagaFinder::class => new CustomDoctrineSagaFinder($this->em)
            ]
        );

        return $setup;
    }

    protected function prepareDb(): void
    {
        $tool = new SchemaTool($this->em);
        $classes = [];
        foreach ($this->setup->getSagaClasses() as $sagaClass) {
            $classes[] = $this->em->getClassMetadata($sagaClass);
        }
        $tool->dropSchema($classes);
        $tool->createSchema($classes);

        $this->em->getConnection()->executeStatement('DROP TABLE IF EXISTS messages_to_send;');
        $this->em->getConnection()->executeStatement(
            'CREATE TABLE IF NOT EXISTS messages_to_send (message_id VARCHAR(36) PRIMARY KEY, message TEXT);'
        );
    }


    protected function sendAllMessages(): void
    {
        $application = $this->application;
        $command = $application->find('p-service-bus:saga:doctrine:outbox:recover-messages');
        $commandTester = new CommandTester($command);

        $commandExitCode = $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );

        static::assertSame(0, $commandExitCode);
    }

    /**
     * @param list<Message<EventOptions>> $messages
     */
    protected function saveMessagesInTable(array $messages): void
    {
        if (count($messages) === 0) {
            return;
        }
        $values = [];
        $parameters = [];
        foreach ($messages as $message) {
            $values[] = "(?, ?)";
            $parameters[] = $message->options->messageId->toString();
            $parameters[] = serialize($message);
        }
        $this->em->getConnection()->executeStatement(
            sprintf(
                "INSERT INTO messages_to_send ('message_id', 'message') VALUES  %s",
                join(',', $values)
            ),
            $parameters
        );
    }
}
