<?php
declare(strict_types=1);

namespace Integration\Handling;

use GDXbsv\PServiceBusTestApp\Handling\Test1Command;
use GDXbsv\PServiceBusTestApp\Handling\TestMultiHandlersCommand;
use GDXbsv\PServiceBusTestApp\Saga\TestSagaCommand;
use GDXbsv\PServiceBusTests\Integration\IntegrationTestCase;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class CommandsTest extends IntegrationTestCase
{
    public function testCommandMultihandler()
    {
        $bus = $this->bus;
        $bus->trace();
        $this->expectException(\RuntimeException::class);
        $commandClass = TestMultiHandlersCommand::class;
        $this->expectExceptionMessage("Command '{$commandClass}' must have only one handler. But we have: 2");
        $bus->send($command = new TestMultiHandlersCommand());
    }
    public function testCommandSucceed()
    {
        $bus = $this->bus;
        $bus->trace();
        $bus->send($command = new Test1Command());
        self::assertEquals('||Test1Command', $this->handlers->result);
    }
}
