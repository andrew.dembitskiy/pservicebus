<?php

declare(strict_types=1);

namespace Integration\Handling;

use GDXbsv\PServiceBusTestApp\Handling\Test1Event;
use GDXbsv\PServiceBusTests\Integration\IntegrationTestCase;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class EventsTest extends IntegrationTestCase
{
    public function testEventSucceed()
    {
        $bus = $this->bus;
        $bus->trace();
        $bus->publish($event = new Test1Event());
        self::assertEquals('||Test1Event||Test1Event', $this->handlers->result);
    }
}
