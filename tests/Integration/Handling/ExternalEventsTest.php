<?php

declare(strict_types=1);

namespace Integration\Handling;

use GDXbsv\PServiceBusTestApp\HandlingExternal\ExternalOutEvent;
use GDXbsv\PServiceBusTests\Integration\IntegrationTestCase;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class ExternalEventsTest extends IntegrationTestCase
{
    public function testEventSucceed()
    {
        $bus = $this->bus;
        $bus->trace();
        $bus->publish($event = new ExternalOutEvent());
        self::assertEquals('||ExternalOutEvent', $this->handlersExternal->result);
        self::assertCount(1, $this->inMemExtTransport->envelopes);
        self::assertEquals('external', $this->inMemExtTransport->envelopes[0]->headers['type']);
        self::assertEquals('test.external_out_event', $this->inMemExtTransport->envelopes[0]->headers['name']);
        self::assertEquals('test.external_out_event', $this->inMemExtTransport->envelopes[0]->headers['name']);
    }
    public function testEventRecieve()
    {
        $bus = $this->bus;
        $bus->trace();
        $bus->publish($event = new ExternalOutEvent());
        self::assertCount(1, $this->inMemExtTransport->envelopes);
        $this->handlersExternal->result = '';
        foreach ($this->bus->consume($this->inMemExtTransport, 1) as $message) {};
        self::assertEquals('||ExternalOutEvent', $this->handlersExternal->result);
    }
}
