<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBusTests\Integration;

use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBus\Bus\TraceableBus;
use GDXbsv\PServiceBus\Setup;
use GDXbsv\PServiceBus\Transport\InMemoryTransport;
use GDXbsv\PServiceBusTestApp\Handling\Handlers;
use GDXbsv\PServiceBusTestApp\Handling\ReplayForEvent;
use GDXbsv\PServiceBusTestApp\HandlingExternal\ExternalOutEvent;
use GDXbsv\PServiceBusTestApp\HandlingExternal\HandlersExternal;
use GDXbsv\PServiceBusTestApp\HandlingExternal\InMemoryExternalTransport;
use GDXbsv\PServiceBusTestApp\HandlingExternal\ReplayForExternalEvent;
use GDXbsv\PServiceBusTestApp\Saga\CustomSagaFinder;
use GDXbsv\PServiceBusTestApp\Saga\TestSaga;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class IntegrationTestCase extends TestCase
{
    protected Setup $setup;
    protected Application $application;
    /** @var Bus\TraceableBus */
    protected Bus $bus;
    protected Handlers $handlers;
    protected HandlersExternal $handlersExternal;
    protected InMemoryTransport $inMemTransport;
    protected InMemoryExternalTransport $inMemExtTransport;

    protected function setUp(): void
    {
        parent::setUp();

        $setup = $this->prepareSetup();
        $setup->build();
        $this->application = $setup->getApplication();
        $this->bus = $setup->getBus();
        $this->init();
        $this->setup = $setup;
    }

    protected function prepareSetup(): Setup
    {
        $inMemTransport = new InMemoryTransport();
        $this->inMemTransport = $inMemTransport;
        $inMemExtTransport = new InMemoryExternalTransport();
        $this->inMemExtTransport = $inMemExtTransport;
        $this->handlers = new Handlers();
        $this->handlersExternal = new HandlersExternal();
        $setup = new Setup(
            [TestSaga::class, $this->handlers::class, $this->handlersExternal::class],
            ['memory' => $inMemTransport, 'memory-external' => $inMemExtTransport],
            [$this->handlers::class => $this->handlers, $this->handlersExternal::class => $this->handlersExternal],
            [ExternalOutEvent::class],
            [ReplayForEvent::class, ReplayForExternalEvent::class],
            [
                ReplayForEvent::class => new ReplayForEvent(),
                ReplayForExternalEvent::class => new ReplayForExternalEvent()
            ],
            [CustomSagaFinder::class],
            [CustomSagaFinder::class => new CustomSagaFinder(),],
            busDecorator: TraceableBus::class
        );

        return $setup;
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    protected function init()
    {
        $application = $this->application;
        $command = $application->find('p-service-bus:init');
        $commandTester = new CommandTester($command);

        $commandExitCode = $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );

        static::assertSame(0, $commandExitCode);
    }

    protected function consume(string $from): int
    {
        $application = $this->application;
        $command = $application->find('p-service-bus:transport:consume');
        $commandTester = new CommandTester($command);

        $commandExitCode = $commandTester->execute(
            [
                'command' => $command->getName(),
                'transport' => $from,
                '--limit' => 50,
            ]
        );

        return $commandExitCode;
    }
}
