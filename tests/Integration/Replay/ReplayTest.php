<?php

declare(strict_types=1);

namespace Integration\Replay;

use GDXbsv\PServiceBusTestApp\Handling\Handlers;
use GDXbsv\PServiceBusTests\Integration\IntegrationTestCase;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class ReplayTest extends IntegrationTestCase
{
    public function testReplayInternal()
    {
        $this->runReplayCommand('testReplay', Handlers::class.'::handle2Event1');
        self::assertEquals('||Test1Event||Test1Event||Test1Event||Test1Event||Test1Event', $this->handlers->result);
    }
    public function testReplayExternal()
    {
        $this->runReplayCommand('testReplayExternal', Handlers::class.'::handle2Event1', external: true);
        self::assertEquals('', $this->handlers->result);
        self::assertCount(5, $this->inMemExtTransport->envelopes);
    }

    protected function runReplayCommand(
        string $replayName,
        string $handler,
        string $transport = 'memory',
        bool $external = false
    ): void {
        $application = $this->application;
        $command = $application->find('p-service-bus:message:replay');
        $commandTester = new CommandTester($command);

        $commandExitCode = $commandTester->execute(
            [
                'replay_name' => $replayName,
                'handler' => $handler,
                'transport' => $transport,
                '--external' => $external,
            ]
        );

        static::assertSame(0, $commandExitCode);
    }
}
