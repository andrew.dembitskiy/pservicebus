<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusTests\Integration\Saga;

use GDXbsv\PServiceBus\Id;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBusTestApp\Saga\CustomDoctrineSearchEvent;
use GDXbsv\PServiceBusTestApp\Saga\TestSaga;
use GDXbsv\PServiceBusTestApp\Saga\TestSagaCommand;
use GDXbsv\PServiceBusTestApp\Saga\TestSagaMapStringCommand;
use GDXbsv\PServiceBusTestApp\Saga\TestsSagaInEvent;
use GDXbsv\PServiceBusTestApp\Saga\TestsSagaOutputEvent;
use GDXbsv\PServiceBusTests\Integration\DoctrineIntegrationTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class SagaDoctrineTest extends DoctrineIntegrationTestCase
{
    public function testSaving()
    {
        $bus = $this->bus;
        $bus->trace();
        $bus->send($command = new TestSagaCommand());
        $this->em->clear();
        /** @var TestSaga $saga */
        $saga = $this->em->getRepository(TestSaga::class)->find(new Id($command->id));
        self::assertEquals($command->string, $saga->string);
    }

    public function testRetriveCustomSaga()
    {
        $bus = $this->bus;
        $command = new TestSagaCommand();
        $command->string = 'customSearchString';
        $bus->send($command);
        $this->em->clear();
        $event = new CustomDoctrineSearchEvent();
        $event->string = 'customSearchString';
        $bus->trace();
        $bus->publish($event);
        self::assertEquals(
            'customSearchString',
            $bus->getEventMessageByClass(TestsSagaOutputEvent::class)->payload->result
        );
    }

    public function testMappingNotId()
    {
        $bus = $this->bus;
        $command1 = new TestSagaCommand();
        $command1->string = 'mappingToString';
        $bus->send($command1);
        $this->em->clear();
        $command2 = new TestSagaMapStringCommand();
        $command2->string = 'mappingToString';
        $bus->trace();
        $bus->send($command2);
        self::assertEquals(
            $command1->id,
            $bus->getEventMessageByClass(TestsSagaOutputEvent::class)->payload->result
        );
    }

    public function testSendAllMessages()
    {
        $bus = $this->bus;
        $this->saveMessagesInTable([new Message(new TestsSagaInEvent(), EventOptions::record())]);
        $bus->trace();
        $this->sendAllMessages();
        self::assertEquals(
            'testListeningFunction',
            $bus->getEventMessageByClass(TestsSagaOutputEvent::class)->payload->result
        );
    }
}
