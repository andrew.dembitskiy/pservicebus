<?php

declare(strict_types=1);

namespace Integration\Transport;

use Bunny\Client;
use GDXbsv\PServiceBus\Transport\Bunny\BunnyFactory;
use GDXbsv\PServiceBus\Transport\Bunny\BunnyTransportExternal;
use GDXbsv\PServiceBus\Transport\Bunny\BunnyTransportInternal;
use GDXbsv\PServiceBus\Transport\Envelope;
use GDXbsv\PServiceBusTestApp\HandlingExternal\ExternalInEvent;
use GDXbsv\PServiceBusTestApp\HandlingExternal\ExternalOutEvent;
use GDXbsv\PServiceBusTests\Integration\IntegrationTestCase;
use Prewk\Result\Err;
use Prewk\Result\Ok;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class BunnyTest extends IntegrationTestCase
{
    public function testSendingInternal()
    {
        $this->cleanQueues(['testQueue']);

        [$channel, $loop] = BunnyFactory::getChannel($this->getConnectionsOptions());

        $transport = new BunnyTransportInternal('testQueue', $channel, $loop);
        $transport->sync();

        $sending = $transport->sending();
        $sending->send(new Envelope(['body1'], 3, 0, []));
        $sending->send(new Envelope(['body2'], 3, 0, []));
        $sending->send(null);

        $envelopesGenerator = $transport->receive(10);

        /** @var list<Envelope> $envelopes */
        $envelopes = [];
        while ($envelopesGenerator->valid()) {
            $envelopes[] = $envelopesGenerator->current();
            $envelopesGenerator->send(new Ok(true));
        }

        self::assertCount(2, $envelopes);
        self::assertEquals(['body1'], $envelopes[0]->payload);
        self::assertEquals(['body2'], $envelopes[1]->payload);
    }

    public function testSendingInternalDLQ()
    {
        $this->cleanQueues(['testQueue']);

        [$channel, $loop] = BunnyFactory::getChannel($this->getConnectionsOptions());

        $transport = new BunnyTransportInternal('testQueue', $channel, $loop);
        $transport->sync();

        $sending = $transport->sending();
        $sending->send(new Envelope(['body1'], 0, 0, []));
        $sending->send(new Envelope(['body2'], 0, 0, []));
        $sending->send(null);

        /** @var list<Envelope> $envelopes */
        $envelopes = [];
        $envelopesGenerator = $transport->receive(4);
        while ($envelopesGenerator->valid()) {
            $envelopes[] = $envelopesGenerator->current();
            $envelopesGenerator->send(new Err(true));
        }

        self::assertCount(2, $envelopes);

        $messages = $this->getFromQueue('testQueue_DL', 4);
        self::assertCount(2, $messages);
    }


    public function testSendingExternal()
    {
        $this->cleanQueues(['external', 'test.external_out_event']);

        [$channel, $loop] = BunnyFactory::getChannel($this->getConnectionsOptions());

        $transport = new BunnyTransportExternal(
            'external',
            $channel,
            $loop,
            ['test.external_out_event' => ExternalOutEvent::class],
            // I use the same name to be able subscribe to myself in real application this should not happened
            ['test.external_out_event' => ExternalInEvent::class]
        );
        $transport->sync();

        $sending = $transport->sending();
        $sending->send(new Envelope(['body1'], 3, 0, ['name' => 'test.external_out_event']));
        $sending->send(null);

        /** @var list<Envelope> $envelopes */
        $envelopes = [];
        $envelopesGenerator = $transport->receive(10);
        while ($envelopesGenerator->valid()) {
            $envelopes[] = $envelopesGenerator->current();
            $envelopesGenerator->send(new Ok(true));
        }

        self::assertCount(1, $envelopes);
        self::assertEquals(['body1'], $envelopes[0]->payload);
    }

    public function testSendingTimeout()
    {
        $this->cleanQueues(['testQueue']);

        [$channel, $loop] = BunnyFactory::getChannel($this->getConnectionsOptions());

        $transport = new BunnyTransportInternal('testQueue', $channel, $loop);
        $transport->sync();

        $sending = $transport->sending();
        $sending->send(new Envelope(['body1'], 0, 2, []));
        $sending->send(null);

        // First time in wait queue
        $messages = $this->getFromQueue('testQueue_TO', 1, true);
        self::assertCount(1, $messages);
        $messages = $this->getFromQueue('testQueue', 1);
        self::assertCount(0, $messages);

        sleep(3);

        // SecondTime in normal queue
        $messages = $this->getFromQueue('testQueue_TO', 1);
        self::assertCount(0, $messages,);
        $messages = $this->getFromQueue('testQueue', 1);
        self::assertCount(1, $messages);
    }

    public function testRetries()
    {
        $this->cleanQueues(['testQueue']);

        [$channel, $loop] = BunnyFactory::getChannel($this->getConnectionsOptions());

        $transport = new BunnyTransportInternal('testQueue', $channel, $loop);
        $transport->sync();

        $sending = $transport->sending();
        $sending->send(new Envelope(['body1'], 1, 2, []));
        $sending->send(null);

        // First time in wait queue
        $envelopesGenerator = $transport->receive(10);
        while ($envelopesGenerator->valid()) {
            $envelopesGenerator->send(new Err(true));
        }

        // First time in wait queue
        $messages = $this->getFromQueue('testQueue_TO', 1, true);
        self::assertCount(1, $messages);
        $messages = $this->getFromQueue('testQueue', 1);
        self::assertCount(0, $messages);

        sleep(2);

        // SecondTime in normal queue
        $messages = $this->getFromQueue('testQueue_TO', 1);
        self::assertCount(0, $messages,);
        $messages = $this->getFromQueue('testQueue', 1, true);
        self::assertCount(1, $messages);

        // Then in DLQ
        $envelopesGenerator = $transport->receive(10);
        while ($envelopesGenerator->valid()) {
            $envelopesGenerator->send(new Err(true));
        }

        // ThirdTime in DLQ
        $messages = $this->getFromQueue('testQueue_DL', 1);
        self::assertCount(1, $messages,);
        $messages = $this->getFromQueue('testQueue', 1, true);
        self::assertCount(0, $messages);
    }

    protected function getConnectionsOptions(): array
    {
        return [
            'host' => 'rabbit',
            'vhost' => 'testvhost',    // The default vhost is /
            'user' => 'testuser', // The default user is guest
            'password' => 'testpassword', // The default password is guest
        ];
    }

    protected function cleanQueues(array $queuesNames): void
    {
        $channel = (new Client($this->getConnectionsOptions()))->connect()->channel();
        foreach ($queuesNames as $name) {
            $channel->queueDelete($name);
            $channel->queueDelete($name . '_DL');
            $channel->queueDelete($name . '_TO');
        }
        $channel->getClient()->disconnect();
    }

    protected function getFromQueue(string $queueName, int $amount, bool $keep = false): array
    {
        $channel = (new Client($this->getConnectionsOptions()))->connect()->channel();
        $messages = [];
        for ($i = 1; $i <= $amount; ++$i) {
            $message = $channel->get($queueName);
            if (!$message) {
                break;
            }
            if ($keep) {
                $channel->reject($message);
            } else {
                $channel->ack($message);
            }
            $messages[] = $message;
        }
        $channel->getClient()->disconnect();

        return $messages;
    }
}
